#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  6 23:16:54 2016

@author: sylvainbertaina
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.constants
              
mpl.rcParams['axes.labelsize']= 18
mpl.rcParams['xtick.labelsize']=14
mpl.rcParams['ytick.labelsize']=14
# Set the default color cycle
colr = '#1b9e77'


file='CW/2012_09_26_AsF6_CW'
h,s=np.loadtxt(file+'169.dat',usecols=(0,1),unpack='True') #EPR
fig, ax = plt.subplots()
figsize=(6,4)
ax.plot(h,s)



ax2 = ax.twiny()

mu_b=scipy.constants.physical_constants['Bohr magneton']
hplanck=scipy.constants.Planck
f_mw=9.43e9

new_tick_locations = np.array([3450,3460,3470])

def tick_function(X):
    V = hplanck*f_mw/(mu_b[0]*X*1e-4)
    return ["%.3f" % z for z in V]

ax2.set_xlim(ax.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel("g factor")
plt.show()